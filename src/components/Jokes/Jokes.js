import React, {Component} from 'react';
import './Jokes.css';

class Jokes extends Component {
    render() {
        return (
            <div className="Jokes">
                <h3 className="Title">Jokes:</h3>
                <p>{this.props.jokes}</p>
            </div>
        );
    }
}

export default Jokes;