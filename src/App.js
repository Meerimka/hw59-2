import React, { Component } from 'react';
import './App.css';
import Jokes from "./components/Jokes/Jokes";
import Button from "./components/Button/Button";

class App extends Component {

  state = {
  jokes:[],
}

    JokeLoad =()=>{
        fetch('https://api.chucknorris.io/jokes/random').then(response =>{
            if(response.ok){
                return response.json()
            }
            throw new Error ('Something wrong with request')
        }).then(jokes =>{
            this.setState({jokes:jokes.value});
        }).catch(error =>{
            console.log(error);
        })
    }

componentDidMount(){
    this.JokeLoad();
}



  render() {
    return (
      <div className="App">
          <Button newjoke={this.JokeLoad}/>
          <Jokes jokes ={this.state.jokes}/>
      </div>
    );
  }
}

export default App;
